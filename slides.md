---
title: Turning out short-notice voting tools in Django for a suddenly-online convention
author: Kim van Wyk
date: 13 October 2022
institute: PyconZA 2022
colortheme: crane
theme: Pittsburgh
urlcolor: red
linkstyle: bold
fonttheme: "professionalfonts"
---

# What is this about?

* The [2021 South African Lions convention](https://www.lions.org.za) moved fully-online with less than 2 months to go.
* Convention includes voting by a subset of attendees
  * 3 votes conducted as secret ballots
  * votes conducted by show of hands at different times, including new items that may be raised during the proceedings
  * No financial stakes at issue in the voting
* Non-exhaustive search for available software or services:
  * Cheaper the better (Lions are a non-profit)
  * Some options required each voter to register on a website and follow a series of moderately complex configuration steps. 
    * Completely secure vote but some users would need tech support. 
  * Many focused on highly cryptographically secure voting and voter secrecy, which made them more complex than I needed.

# Chosen Approach

* Decided to roll my own with Django, splitting the problem in 2:
  * tooling that could allow secret balloting before the convention
  * tooling to present options for voting on during the convention. 
* *secretish* was good enough for my needs 
  * a determined sysadmin could still see the data, but no one else

# What will I cover?

* Using Django to rapidly get a no-frills site up and running
* leveraging the Django admin backend to avoid having to write large parts of such systems
* an approach to being secret enough while remaining practical and supportable by 1 IT volunteer with a full-time unrelated job (me)
* being flexible enough to preload the questions that will require show-of-hands voting and allow new items to be added at short notice during the convention
* rapid deployment with Docker

# Secretish Balloting

[gitlab.com/kimvanwyk/secretish-balloting](https://gitlab.com/kimvanwyk/secretish-balloting)

Some design decisions:

* If the developer is the sysadmin, the basic Django admin is enough
  * Also allows for simplistic but non-obvious solutions if that makes sense 
* An all-powerful admin user with a known static password is sufficient
* With a closed group of users with known expectations, you can get away with 1995-era table-heavy HTML and no styling

# Design Decisions

* Each voter could be issued a username and password - but that requires supporting infrastructure and a support person
  * Sending each voter a URL via email with a random 8-digit value in its path ensures "good enough" uniqueness
* BCCing the sysadmin into each email makes support a matter of forwarding the email
* Similarly, one or more observers need access to the ballot results
  * A random 8-digit value in that URL is also unguessable enough to meet the need
  
# Unique URL Generation

Django models make this kind of thing pleasantly easy:

\scriptsize
```python
from django.db import models
...
class Voter(models.Model):
   ...

   url_fragment_text = models.CharField(
        max_length=20, unique=True, null=True, blank=True, default=None
    )

    def save(self, *args, **kwds):
        if self.url_fragment_text is None:
            fragments = [o.url_fragment_text for o in Voter.objects.all()]
            while True:
                self.url_fragment_text = random.randint(10000000, 99999999)
                if self.url_fragment_text not in fragments:
                    break
        super().save(*args, **kwds)
```

\small

# Demo

Enough waffle for a bit, demo goes here.


# Conference Open Voting

[gitlab.com/kimvanwyk/conference-open-voting](https://gitlab.com/kimvanwyk/conference-open-voting)

The design decisions were much the same as the secretish balloting

* Simple coding making life a little harder for the sysadmin
* Minimal complexity for the user
* Convention delegates receive a unique registration number, which made a suitable ID for granting voting permissions
  * Another "good enough" situation - little protection against impersonation or guessing of IDs but could trust it not to be a problem
